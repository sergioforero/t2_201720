package model.vo;

import model.data_structures.DoubleLinkedList;

/**
 * Representation of a route object
 */
public class VORoute {

	public String id;
	public String agency;
	public String shortname;
	public String longname;
	public String desc;
	public String type;
	public String url;
	public String color;
	public String textcolor;
	
	public VORoute(String a, String b, String c, String d, String e, String f, String g, String h, String i)
	{
		id = a;
		agency = b;
		shortname = c;
		longname = d;
		desc = e;
		type = f;
		url = g;
		color = h;
		textcolor = i;
	}
	
	/**
	 * @return id - Route's id number
	 */
	public int id() {
		// TODO Auto-generated method stub
		return Integer.parseInt(id);
	}

	/**
	 * @return name - route name
	 */
	public String getName() {
		// TODO Auto-generated method stub
		return longname;
	}
	
	

}
