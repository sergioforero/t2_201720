package model.vo;

import model.data_structures.DoubleLinkedList;
import model.data_structures.RingList;

/**
 * Representation of a Stop object
 */
public class VOStop {

	public String id;
	public String code;
	public String name;
	public String desc;
	public String lat;
	public String lon;
	public String zoneid;
	public String url;
	public String locationtype;
	public String parentstation;
	
	public VOStop(String a, String b, String c, String d, String e, String f, String g, String h, String i, String j)
	{
		id = a;
		code = b;
		name = c;
		desc = d;
		lat = e;
		lon = f;
		zoneid = g;
		url = h;
		locationtype = i;
		parentstation = j;
	}
	
	/**
	 * @return id - stop's id
	 */
	public int id() {
		// TODO Auto-generated method stub
		return Integer.parseInt(id);
	}

	/**
	 * @return name - stop name
	 */
	public String getName() {
		// TODO Auto-generated method stub
		return name;
	}

}
