package model.vo;

public class VOTrip 
{
	public String route_id;
	public String service_id;
	public String trip_id;
	public String trip_headsign;
	public String trip_short_name;
	public String direction_id;
	public String block_id;
	public String shape_id;
	public String wheelchair_accessible;
	public String bikes_allowed;
	
	public VOTrip(String a, String b, String c, String d, String e, String f, String g, String h, String i, String j)
	{
		route_id = a;
		service_id = b;
		trip_id = c;
		trip_headsign = d;
		trip_short_name = e;
		direction_id = f;
		block_id = g;
		shape_id = h;
		wheelchair_accessible = i;
		bikes_allowed = j;
	}
}

