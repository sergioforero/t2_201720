package model.vo;

public class VOStopTime 
{
	public String trip_id;
	public String arrival_time;
	public String departure_time;
	public String stop_id;
	public String stop_sequence;
	public String stop_headsign;
	public String pickup_type;
	public String drop_off_type;
	public String shape_dist_traveled;
	
	public VOStopTime(String a, String b, String c, String d, String e, String f, String g, String h, String i)
	{
		trip_id = a;
		arrival_time = b;
		departure_time = c;
		stop_id = d;
		stop_sequence = e;
		stop_headsign = f;
		pickup_type = g;
		drop_off_type = h;
		shape_dist_traveled = i;
	}
}
