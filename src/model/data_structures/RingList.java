package model.data_structures;

import java.util.HashSet;
import java.util.Iterator;





public class RingList<T > implements IList<T>
{

	public Node <T> first;
	public Node <T> last;
	public RingList()
	{
		
		first = null;
		last = first;
		
	}
	
	
	@Override
	public Iterator<T> iterator()
	{
		// TODO Auto-generated method stub
		return new ListIterator();
	}
	private class ListIterator implements Iterator<T>
	{
        private Node<T> current = first;
        
		@Override
		public boolean hasNext() 
		{
		   	
			return current!= first;
		}

		@Override
		public T next()
		{
			// TODO Auto-generated method stub
			Node<T> temp = current;
		     current = current.darSiguiente();
			return temp.objeto;
		}
		
	}

	@Override
	public int getSize() {
		// TODO Auto-generated method stub
		int size = 0;
		Iterator<T> iter = iterator();
		boolean encontrado = false;
		while (!encontrado && first != null)
		{
			
			size++;
		  T current = iter.next();
		  if (current.equals(last.objeto))
		  {
			  encontrado = true;
		  }
		
		}
		return size;
		
	}

	@Override
	public void addAtEnd(T aAgregar) 
	{
		if (isEmpty())
		{
			Node<T> temp = new Node(aAgregar,last,last);
			first = temp;
			last = first;
		}
		
		else
		{
	    Node<T> oldLast = last;
		Node<T> nuevo = new Node<T>(aAgregar,oldLast,first);
		oldLast.cambiarSiguiente(nuevo);
		first.cambiarAnterior(nuevo);
		last = nuevo;
		}
	}

	@Override
	public void AddAtK(T anterior,T objeto) 
	{
	   if(!isEmpty())
	   {
		// TODO Auto-generated method stub
		Node<T> actual = first;
		Iterator<T> iter = iterator();
		boolean encontrado = false;
		boolean completo = false;
		while (!encontrado &&!completo )
		{
			T current = iter.next();
			if (current.equals(anterior))
			{
				 Node<T> nuevo = new Node<T>(objeto,actual,actual.darSiguiente());
			 actual.cambiarSiguiente(nuevo);
			 if(last.darAnterior()==null)
			 {
				 nuevo.cambiarSiguiente(first);
				  last = nuevo;
				 first.cambiarSiguiente(nuevo);
				  first.cambiarAnterior(nuevo);
				 
			 }
			 else
			 {
				 actual.darSiguiente().cambiarAnterior(nuevo);
			 }
				  encontrado = true;
			}
			else
			{
			actual = actual.darSiguiente();
		    }
			if (current.equals(last.objeto))
			{
			   completo = true;
			}
		}
	
	   }
	}

	@Override
	public Node<T> getElement(T element) 
	{
		Node<T> actual = first;
		Iterator<T> iter = iterator();
		boolean encontrado = false;
		boolean completo = false;
		while (!encontrado && !completo)
		{
			T current = iter.next();
			if (current.equals(element))
			{
				encontrado = true;
			}
			else
			{
			actual = actual.darSiguiente();
			}
			if (current.equals(last.objeto))
			{
				completo = true;
			}
			}
		if (encontrado)
		{
			return actual;
		}
		else return null;
	}
	

	@Override
	public T getCurrentElement(Node<T> nodo) 
	{
		// TODO Auto-generated method stub
		return nodo.objeto;
	}

	@Override
	public void delete() 
	{
		if (!isEmpty())
		{
		if (first.darSiguiente()==null)
		{
			first = null;
			last = null;
		}
		else
		{
		first = first.darSiguiente();
		first.cambiarAnterior(last);
		last.cambiarSiguiente(first);
		}
		}
	}

	@Override
	public void deleteAtK(T aEliminar) 
	{
		if(!isEmpty())
		{
		Node<T> actual = first;
		Iterator<T> iter = iterator();
		boolean encontrado = false;
		boolean completo = false;
		while (!encontrado && !completo)
		{
			T current = iter.next();
			if (current.equals(aEliminar))
			{
				
				actual.darAnterior().cambiarSiguiente(actual.darSiguiente());
			    actual.darSiguiente().cambiarAnterior(actual.darAnterior());
				encontrado = true;
			}
			else
			{
			actual = actual.darSiguiente();
			}
		if (current.equals(last.objeto))
		{
			completo = true;
		}
		}
	
		}
	}

	@Override
	public T next(Node<T> nodo)
	{
		// TODO Auto-generated method stub
		return nodo.darSiguiente().darObjeto();
	}

	@Override
	public T previous(Node<T> nodo) {
		// TODO Auto-generated method stub
		return nodo.darAnterior().darObjeto();
	}

	@Override
	public void add(T aAgregar)
	{
		if (isEmpty())
		{
			Node<T> temp = new Node(aAgregar,null,null);
			first = temp;
			last = first;
		}
		else
		{
		
		Node<T> oldFirst = first;
		Node<T> nuevo = new Node<T>(aAgregar,last,oldFirst);
		oldFirst.cambiarAnterior(nuevo);
		last.cambiarSiguiente(nuevo);
		first = nuevo;
		
		// TODO Auto-generated method stub
		}
	}
	public boolean isEmpty()
	{
		return first == null;
	}
	
	
	


}
