package model.data_structures;

/**
 * Abstract Data Type for a list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface IList<T> extends Iterable<T>
{

public int getSize();



public void addAtEnd(T objeto);

public void AddAtK(T anterior, T objeto);

public Node<T> getElement(T element);

public T getCurrentElement(Node<T> nodo);

public void delete();

 public void deleteAtK(T aEliminar);

 public T next(Node<T> nodo);


public T previous(Node <T> nodo);


public void add(T objeto);


}