package model.data_structures;

import java.util.HashSet;
import java.util.Iterator;

import model.data_structures.Node;

public class DoubleLinkedList< T >  implements IList <T>

{
	public Node<T> first;
	public Node<T> last;
	
	
	public  DoubleLinkedList()
	{
		first = null;
		last = first;
		
	}
	@Override
	public int getSize() 
	{
		int size = 0;
		Iterator<T> iter = iterator();
		while (iter.hasNext())
		{
			size++;
			iter.next();
		}
		return size;
		
	}
	@Override
	public void addAtEnd(T aAgregar) 
	{
		if (isEmpty())
		{
			Node<T> temp = new Node<T>(aAgregar,null,null);
			last = temp;
			first  = last;
		}
		else
		{
		
		Node<T> oldLast = last;
		Node<T> nuevo = new Node<T>(aAgregar,oldLast,null);
		oldLast.cambiarSiguiente(nuevo);
		last = nuevo;
		if (first.darSiguiente() == null)
		{
			first = oldLast;
		}
		}
	}

	


	@Override
	public Node<T> getElement(T element) 
	{
		Node<T> actual = first;
		Iterator<T> iter = iterator();
		boolean encontrado = false;
		while (!encontrado&& iter.hasNext())
		{
			T current = iter.next();
			if (current.equals(element))
			{
				encontrado=true;
			}
			else
			{
			actual = actual.darSiguiente();
			
			}
			}
		if (!encontrado)
		{
			return null;
		}
		else return actual;
	}

	@Override
	public T getCurrentElement(Node<T> nodo) 
	{
		return nodo.objeto;
	              
	}

	@Override
	public void delete() 
	{
		
	if (!isEmpty())
	{
		if (first.darSiguiente() == null)
		{
			first = null;
			last = first;
		}
		
	else
	{
		first = first.darSiguiente();
		first.cambiarAnterior(null);
	}
	}	
		// TODO Auto-generated method stub
	}



	





	@Override
	public void add(T aAgregar) 
	{
	 if (isEmpty())
	 {
		Node<T> temp = new Node(aAgregar,null,null);
		first = temp;
		last = first;
	 }
	 
	 else
	 {
		 Node<T> temp = new Node(aAgregar,null,first);
		 first.cambiarAnterior(temp);
		 Node<T> oldFirst = first;
		 first = temp;
		 if (last.darAnterior() == null)
		 {
			 last = oldFirst;
		 }
	 }
	 
		
	}
	@Override
	public void AddAtK(T anterior, T objeto)
	{
		if (!isEmpty())
		{
		Node<T> actual = first;
		Iterator<T> iter = iterator();
		boolean encontrado = false;
		while (!encontrado && iter.hasNext())
		{
			 T current = iter.next();
			 
			if (current.equals(anterior))
			{
				 Node<T> nuevo = new Node<T>(objeto,actual,actual.darSiguiente());
				  actual.cambiarSiguiente(nuevo);
				
				  if (last.darAnterior() == null)
				  {
					  last = nuevo;
					  
			      }
				  else
				  {
					  nuevo.darSiguiente().cambiarAnterior(nuevo);
				  }
				  encontrado = true;
			}
			else 
			{
			
			
			actual = actual.darSiguiente();
			
			}
			}
		}

	}
	@Override
	public void deleteAtK(T aEliminar) 
	{
		if (!isEmpty())
		{
		
		// TODO Auto-generated method stub
		Node<T> actual = first;
		Iterator<T> iter = iterator();
		boolean encontrado = false;
		while (!encontrado&& iter.hasNext())
		{
			T current = iter.next();
			if (current.equals(aEliminar))
			{
				 actual.darAnterior().cambiarSiguiente(actual.darSiguiente());
			      actual.darSiguiente().cambiarAnterior(actual.darAnterior());
			    encontrado = true;
		           
			
			}
			else
			{
			actual = actual.darSiguiente();
			}
		}
	
	
		}
	}
	

	@Override
	public T next(Node<T> actual) 
	{
		// TODO Auto-generated method stub
		return actual.darSiguiente().darObjeto();
	}
	@Override
	public T previous(Node<T> actual) {
		// TODO Auto-generated method stub
		return actual.darAnterior().darObjeto();
	}





	@Override
	public Iterator<T> iterator() 
	{
		return new ListIterator();
	}
	private class ListIterator implements Iterator<T>
	{
       public Node<T> current = first;
		@Override
		public boolean hasNext() {
			// TODO Auto-generated method stub
			return current != null;
		}

		@Override
		public T next() {
		     Node<T> temp = current;
		     current = current.darSiguiente();
			return temp.objeto;
		}
		
	}
	
public boolean isEmpty()
{
	return first ==null;
}

	






	





	
}