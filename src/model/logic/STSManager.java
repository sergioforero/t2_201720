package model.logic;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;

import api.ISTSManager;
import model.vo.VORoute;
import model.vo.VOStop;
import model.vo.VOStopTime;
import model.vo.VOTrip;
import model.data_structures.DoubleLinkedList;
import model.data_structures.IList;
import model.data_structures.Node;
import model.data_structures.RingList;

public class STSManager implements ISTSManager {

	private DoubleLinkedList<VORoute> routes;
	private RingList<VOTrip> trips;
	private DoubleLinkedList<VOStopTime> stoptimes;
	private RingList<VOStop> stops;
	
	@Override
	public void loadRoutes(String routesFile) {
		// TODO Auto-generated method stub
		try
		{
			BufferedReader bf = new BufferedReader(new FileReader(routesFile));
			String line = bf.readLine();
			routes = new DoubleLinkedList<VORoute>(null, null);
			while((line = bf.readLine()) != null)
			{
				String[] data = line.split(",");
				VORoute route = new VORoute(data[0], data[1], data[2], data[3], data[4], data[5], data[6], data[7], data[8]);
				routes.addAtEnd(route);
			}
			bf.close();
		}
		catch(IOException ioe)
		{
			System.out.print("IO Exception");
		}
	}

	@Override
	public void loadTrips(String tripsFile) {
		// TODO Auto-generated method stub
		try
		{
			BufferedReader bf = new BufferedReader(new FileReader(tripsFile));
			String line = bf.readLine();
			trips = new RingList<VOTrip>(null, null);
			int routeid = -1;
			int tripid = -1;
			while((line = bf.readLine()) != null)
			{
				String[] data = line.split(","); 
				if(Integer.parseInt(data[0]) == routeid)
				{
					if(Integer.parseInt(data[2]) > tripid)
					{
						VOTrip trip = new VOTrip(data[0], data[1], data[2], data[3], data[4], data[5], data[6], data[7], data[8], data[9]);
						trips.addAtEnd(trip);
						tripid = Integer.parseInt(data[2]);
					}
					else
					{
						Iterator<VOTrip> tripsit = trips.iterator();
						VOTrip a = tripsit.next();
						while(tripsit.hasNext() && a.route_id != data[0])
						{
							a = tripsit.next();
						}
						while(tripsit.hasNext() && Integer.parseInt(a.trip_id) > Integer.parseInt(data[2]))
						{
							a = tripsit.next();
						}
						VOTrip trip = new VOTrip(data[0], data[1], data[2], data[3], data[4], data[5], data[6], data[7], data[8], data[9]);
						trips.AddAtK(a, trip);
					}
				}
				else
				{
				VOTrip trip = new VOTrip(data[0], data[1], data[2], data[3], data[4], data[5], data[6], data[7], data[8], data[9]);
				trips.addAtEnd(trip);
				routeid = Integer.parseInt(data[0]);
				tripid = -1;
				}
			}
			bf.close();
		}
		catch(IOException ioe)
		{
			System.out.print("IO Exception");
		}
	}

	public void loadStopTimes(String stopTimesFile) {
		// TODO Auto-generated method stub
		try
		{
			BufferedReader bf = new BufferedReader(new FileReader(stopTimesFile));
			String line = bf.readLine();
			stoptimes = new DoubleLinkedList<VOStopTime>(null, null);
			while((line = bf.readLine()) != null)
			{
				String[] data = line.split(",");
				VOStopTime time = new VOStopTime(data[0], data[1], data[2], data[3], data[4], data[5], data[6], data[7], data[8]);
				stoptimes.addAtEnd(time);
			}
			bf.close();
		}
		catch(IOException ioe)
		{
			System.out.print("IO Exception");
		}
	}

	@Override
	public void loadStops(String stopsFile) {
		// TODO Auto-generated method stub
		try
		{
			BufferedReader bf = new BufferedReader(new FileReader(stopsFile));
			String line = bf.readLine();
			stops = new RingList<VOStop>(null, null);
			int zoneid = 0;
			int stopid = -1;
			while((line = bf.readLine()) != null)
			{
				String[] data = line.split(","); 
				String[] z = data[6].split(" ");
				zoneid = Integer.parseInt(z[1]);
				if(Integer.parseInt(data[6]) == zoneid)
				{
					if(Integer.parseInt(data[0]) > stopid)
					{
						VOStop stop = new VOStop(data[0], data[1], data[2], data[3], data[4], data[5], data[6], data[7], data[8], data[9]);
						stops.addAtEnd(stop);
						stopid = Integer.parseInt(data[0]);
					}
					else
					{
						Iterator<VOStop> stopsit = stops.iterator();
						VOStop a = stopsit.next();
						while(stopsit.hasNext() && a.zoneid != data[6])
						{
							a = stopsit.next();
						}
						while(stopsit.hasNext() && Integer.parseInt(a.zoneid) > Integer.parseInt(data[0]))
						{
							a = stopsit.next();
						}
						VOStop stop = new VOStop(data[0], data[1], data[2], data[3], data[4], data[5], data[6], data[7], data[8], data[9]);
						stops.AddAtK(a, stop);
					}
				}
				else
				{
				VOTrip trip = new VOTrip(data[0], data[1], data[2], data[3], data[4], data[5], data[6], data[7], data[8], data[9]);
				trips.addAtEnd(trip);
				zoneid = Integer.parseInt(data[6]);
				stopid = -1;
				}
			}
			bf.close();
		}
		catch(IOException ioe)
		{
			System.out.print("IO Exception");
		}
	}

	@Override
	public IList<VORoute> routeAtStop(String stopName) {
		// TODO Auto-generated method stub
		DoubleLinkedList<VORoute> routeAtStop = new DoubleLinkedList<VORoute>(null, null);
		Iterator<VOStop> rt = stops.iterator();
		VOStop actual = null;
		while(rt.hasNext())
		{
			actual = rt.next();
			if(actual != null && actual.getName().contains(stopName) || 
					stopName.contains(actual.getName()))
			{
				Iterator<VOTrip> trp = trips.iterator();
				VOTrip act = null;
				while(trp.hasNext())
				{
					act = trp.next();
					boolean t = false;
					if(actual.code.equals("90"+act.trip_id) && !t)
					{
						Iterator<VORoute> rts = routes.iterator();
						VORoute route = null;
						while(rts.hasNext() && !t)
						{
							route = rts.next();
							if(act.trip_id.equals(route.id()))
							{
								routeAtStop.add(route);
								t=true;
							}
						}
						t=true;
					}
				}
			}
		}
		return routeAtStop;
	}

	@Override
	public IList<VOStop> stopsRoute(String routeName, String direction) {
		// TODO Auto-generated method stub
		DoubleLinkedList<VOStop> stopsroute = new DoubleLinkedList<VOStop>(null, null);
		Iterator<VORoute> rt = routes.iterator();
		VORoute actual = null;
		boolean t = false;
		while(rt.hasNext() && !t)
		{
			actual = rt.next();
			if(actual != null && actual.getName().equals(routeName))
			{
				t = true;
			}
		}
		Iterator<VOTrip> trp = trips.iterator();
		VOTrip act = null;
		while(trp.hasNext())
		{
			act = trp.next();
			if(actual.id.equals(act.route_id))
			{
				Iterator<VOStop> stp = stops.iterator();
				VOStop stop = null;
				while(stp.hasNext())
				{
					stop = stp.next();
					if(act.trip_id.equals("90"+stop.code))
					{
						stopsroute.add(stop);
					}
				}
			}
		}
		return stopsroute;
	}

}
