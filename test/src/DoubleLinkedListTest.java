package src;

import static org.junit.Assert.*;
import junit.framework.TestCase;
import model.data_structures.DoubleLinkedList;
import model.data_structures.Node;

import org.junit.Test;

public class DoubleLinkedListTest<T> extends TestCase
{
  private DoubleLinkedList lista;
  
 
  
  
  public void escenario()
  {
	 
	  lista = new DoubleLinkedList();
	

  }
  public void testDoubleLinkedList()
  {
	  escenario();
	  assertNull ("No se creo la lista correctamente",lista.first);
	  assertNull ("No se creo la lista correctamente",lista.last);
	  
  }
  public void testGetSize()
  {
	  escenario();
	  assertEquals("No conto los elementos correctamente",0,lista.getSize());
	  lista.add("A");
	  lista.add("B");
	  lista.add("C");
	  assertEquals("No conto los elementos correctamente",3,lista.getSize());
  }
  public void testAdd()
  {
	  escenario();
	  lista.add("A");
	  assertEquals("No agrego el elemento correctamente","A",lista.first.objeto);
	  lista.add("B");
	  assertEquals("No agrego el elemento correctmanete","B",lista.first.objeto);
	  
  }
  public void testAddAtEnd()
  {
	  escenario();
	  lista.addAtEnd("A");
	  assertEquals("No agrego el elemento correctamente","A",lista.last.objeto);
	  lista.addAtEnd("B");
	  assertEquals("No agrego el elemento correctamente","B",lista.last.objeto);
  }
  public void testAddAtK()
  {
	  escenario();
	  lista.AddAtK("A", "B");
      assertNull("No debio agregar el elemento",lista.first);
      lista.add("A");
      lista.AddAtK("A", "B");
      assertEquals("No agrego el elemento correctamente","B",lista.last.objeto);
  }
  public void testDelete()
  {
	  escenario();
	  lista.add("A");
	  lista.delete();
	  assertNull("Deberia estar vacia la lista",lista.first);
	  lista.add("A");
	  lista.add("B");
	  lista.delete();
	  assertEquals("No borro el elemento correctamente","A",lista.first.objeto);
	  
  }
  public void testDeleteAtK()
  {
	  escenario();
	  lista.deleteAtK("A");
	  assertNull("Debeira seguir vacia la lista",lista.first);
	  lista.add("c");
	  lista.add("b");
	  lista.add("a");
	  lista.deleteAtK("b");
	  assertEquals("No elimino el elemento correctamente","c",lista.first.darSiguiente().objeto);
  }
 
	
}
